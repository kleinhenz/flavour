package de.storyquest.client.flavour.mobile;

import android.util.Log;
import com.google.android.gms.wearable.MessageEvent;
import com.google.android.gms.wearable.WearableListenerService;

public class ListenerService extends WearableListenerService {

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.i("Flavour", messageEvent.getPath());
        Log.i("Flavour", new String(messageEvent.getData()));
    }
}
