package de.storyquest.client.flavour.mobile;

import android.os.Handler;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.Node;
import com.google.android.gms.wearable.NodeApi;
import com.google.android.gms.wearable.Wearable;

import java.util.List;
import java.util.concurrent.TimeUnit;

import static android.view.View.OnClickListener;


public class FlavourMobileActivity extends ActionBarActivity {

    private String nodeId;
    private TextView nodeIdLabel;
    private GoogleApiClient client;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_flavour_mobile);

        nodeIdLabel = (TextView) findViewById(R.id.nodeIdLabel);
        Button notifyButton = (Button) findViewById(R.id.notify);
        notifyButton.setOnClickListener(new OnClickListener() {
            @Override
            public void onClick(View v) {
                notifyWearable();
            }
        });

        client = createGoogleAPIClient();
        retrieveDeviceNode(client);
    }

    private void notifyWearable() {
        String message = "Hello World!";

        sendMessage(client, message);
    }

    private GoogleApiClient createGoogleAPIClient() {
        return new GoogleApiClient.Builder(this).addApi(Wearable.API).build();
    }

    private void sendMessage(final GoogleApiClient client, final String message) {
        if (nodeId != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    Log.i("FlavourMobile", "Send message " + message);
                    client.blockingConnect(10l, TimeUnit.SECONDS);
                    Wearable.MessageApi.sendMessage(client, nodeId, message, message.getBytes());
                    client.disconnect();
                    Log.i("FlavourMobile", "Message send!");
                }
            }).start();
        }
    }

    private void retrieveDeviceNode(final GoogleApiClient client) {
        final Handler handler = new Handler(this.getMainLooper());

        new Thread(new Runnable() {
            @Override
            public void run() {
                Log.i("Node-ID", "Retreiving device nodes...");
                client.blockingConnect(10l, TimeUnit.SECONDS);
                NodeApi.GetConnectedNodesResult result = Wearable.NodeApi.getConnectedNodes(client).await();

                List<Node> nodes = result.getNodes();

                if (nodes.size() > 0) {
                    for (Node node : nodes) {
                        Log.i("Node-ID", node.getDisplayName() + " [" + node.getId() + "]");
                    }
                    nodeId = nodes.get(0).getId();


                    handler.post(new Runnable() {
                        @Override
                        public void run() {
                            nodeIdLabel.setText(nodeId);
                         }
                    });
                }
                Log.i("Node-ID", "Done!");
                client.disconnect();
            }
        }).start();
    }



    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_flavour_mobile, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
