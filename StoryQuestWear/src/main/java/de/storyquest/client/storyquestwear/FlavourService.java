package de.storyquest.client.storyquestwear;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.graphics.BitmapFactory;
import android.support.wearable.view.CardFragment;
import android.util.Log;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.wearable.*;

import java.util.List;
import java.util.concurrent.TimeUnit;

public class FlavourService extends WearableListenerService {

    protected final static String TAG = "FlavourService";
    protected final static long CONNECTION_TIME_OUT_MS = 5000;

    protected GoogleApiClient mGoogleApiClient = null;
    protected String peerNode = null;

    @Override
    public void onCreate() {
        super.onCreate();
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addApi(Wearable.API)
                .build();
        mGoogleApiClient.connect();
        Log.i(TAG, "Starting Flavour service on Android Wearable.");
        retrieveDeviceNode();
        sendMessage("Das hier kommt vom Wearable START..!");
    }

    @Override
    public void onMessageReceived(MessageEvent messageEvent) {
        Log.i(TAG, "Message received: " + new String(messageEvent.getData()));

        sendMessage("Das hier kommt vom Wearable MESSAGE RECEIVED..!");
        /*
        // Build the intent to display our custom notification
        Intent notificationIntent =
                new Intent(this, CardFragment.class);
        notificationIntent.putExtra(
                CardFragment.KEY_TITLE, "TITLE NOT");
        notificationIntent.putExtra(
                CardFragment.KEY_TEXT, "TEXT NOT");
        PendingIntent notificationPendingIntent = PendingIntent.getActivity(
                this,
                0,
                notificationIntent,
                PendingIntent.FLAG_UPDATE_CURRENT);

        // Create the ongoing notification
        Notification.Builder notificationBuilder =
                new Notification.Builder(this)
                        .setSmallIcon(R.drawable.ic_launcher)
                        .setLargeIcon(BitmapFactory.decodeResource(
                                getResources(), R.drawable.ic_launcher))
                        .extend(new Notification.WearableExtender()
                                .setDisplayIntent(notificationPendingIntent));
        Log.i(TAG, "SHOW NOTIFICATION");

        // Build the notification and show it
        NotificationManager notificationManager =
                (NotificationManager)getSystemService(NOTIFICATION_SERVICE);
        notificationManager.notify(42, notificationBuilder.build());
        */
    }

    protected void retrieveDeviceNode() {
        new Thread(new Runnable() {
            @Override
            public void run() {
                mGoogleApiClient.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                NodeApi.GetConnectedNodesResult result =
                        Wearable.NodeApi.getConnectedNodes(mGoogleApiClient).await();
                List<Node> nodes = result.getNodes();
                if (nodes.size() > 0) {
                    FlavourService.this.peerNode = nodes.get(0).getId();
                }
                Log.i(TAG, "FOUND DEVICE NODE: " + nodes.get(0).getId());
            }
        }).start();
    }

    public void sendMessage(final String message) {
        if (peerNode != null) {
            new Thread(new Runnable() {
                @Override
                public void run() {
                    mGoogleApiClient.blockingConnect(CONNECTION_TIME_OUT_MS, TimeUnit.MILLISECONDS);
                    Wearable.MessageApi.sendMessage(mGoogleApiClient, peerNode, message, null);
                    mGoogleApiClient.disconnect();
                }
            }).start();
        }
    }
}
